widgetNameIntr = function() {
    var widget = this;
    this.code = null;

    this.yourVar = {};
    this.yourFunc = function() {};

// вызывается один раз при инициализации виджета, в этой функции мы вешаем события на $(document)
    this.bind_actions = function(){
//пример $(document).on('click', 'selector', function(){});
    };

// вызывается каждый раз при переходе на страницу
    this.render = function() {



            let pipe = $('.pipeline_wrapper')[0]['children'];
            let arr = [];
            for (key in pipe) {
                arr.push(pipe[key]);
                if (arr.length == 4) break;
            }
            for (let i = 0; i < arr[0].classList.length; i++) {
                if (arr[0].classList[i] == 'h-hidden') {
                    arr.shift();
                }
            }
            let color = arr[2].children[0].children[0].children[2].style.color;
            let text = arr[2].children[0].children[0].children[0].children[0].style.color = color;
        
    };

// вызывается один раз при инициализации виджета, в этой функции мы загружаем нужные данные, стили и.т.п
    this.init = function(){

    };

// метод загрузчик, не изменяется
    this.bootstrap = function(code) {
        widget.code = code;
// если frontend_status не задан, то считаем что виджет выключен
// var status = yadroFunctions.getSettings(code).frontend_status;
        var status = 1;

        if (status) {
            widget.init();
            widget.render();
            widget.bind_actions();
            $(document).on('widgets:load', function () {
                widget.render();
            });
        }
    }
};
// создание экземпляра виджета и регистрация в системных переменных Yadra
// widget-name - ИД и widgetNameIntr - уникальные названия виджета
yadroWidget.widgets['varonkaColor'] = new widgetNameIntr();
yadroWidget.widgets['varonkaColor'].bootstrap('varonkaColor');